#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char * argv[]){
        int i=0;
        char buffer[100];
        memset(buffer,0,100);
        for(i=1;i<argc;i++){
                pid_t id = fork();
                if(id==0){
                        snprintf(buffer,100,"%d.png",i);
                        execl("procesador_png","procesador_png", argv[i],buffer,NULL);
                        return 0;
                }
                else{
			int status;
                        pid_t muerto = wait(&status);
                        printf("%s (archivo BW %d.png) : status %d  hijo %d\n", argv[i],i,WEXITSTATUS(status),muerto);
                }
        }

        return 0;
}

